<?php
require_once ("../library/define.php");
class PushNotification {
  // constructor
  function __construct() {

  }
  /**
   * Sending Push Notification
   */
  public function push_notification($registatoin_ids, $message) {
    // Set POST variables
    $url = "https://fcm.googleapis.com/fcm/send";
    $fields = array(
        'registration_ids' => $registatoin_ids,
        'notification' => $message,
        'priority' => 'high'
    );
    $headers = array(
        'authorization: key=' . GOOGLE_API_KEY,
        "cache-control: no-cache",
        "content-type: application/json"
    );
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($fields),
      CURLOPT_HTTPHEADER => $headers,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }
}
?>
