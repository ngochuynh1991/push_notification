<?php
require_once 'define.php';
require_once 'functions.php';
class DB {

    private $myconn;

    // constructor
    function __construct() {
      // connecting to mysql
      $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
      if (!$conn) {
          status500('Cannot connect to database!');
      } else {
          mysqli_set_charset($conn,"utf8");
          $this->myconn = $conn;
      }
    }

    // destructor
    function __destruct() {

    }
    /*
       Function: check_uuid
       Check isset uuid from users table.
    */
    function check_uuid($uuid) {
        $q = "SELECT id FROM users WHERE uuid='$uuid' AND status=1 LIMIT 1";
        $r = mysqli_query($this->myconn, $q) or die(mysqli_error($this->myconn));
        if(mysqli_num_rows($r) > 0) {
            return true;
        }
        return false;
    }

    /*
       Function: getByUid
       Get one record from users table by uuid.
    */
    function getByUid($uuid) {
      $result = array('status' => ERROR_NOT_FOUND);
      $q = "SELECT token FROM users WHERE uuid='$uuid' LIMIT 1";
      if(mysqli_query($this->myconn, $q)) {
        $r = mysqli_query($this->myconn, $q);
        if (mysqli_num_rows($r) > 0) {
          $row = mysqli_fetch_assoc($r);
          $result = array(
              'status' => SUCCESS_OK,
              // 'token' => $row['token']
          );
        }
      }
      return $result;
    }

    /*
       Function: saveUserByParams
       Insert record with input is an array.
    */
    function saveUserByParams($params) {
      $data = array('status' => ERROR_NOT_FOUND);
      $uuid   = !empty($params->uuid)  ? $params->uuid : '';
      $name   = !empty($params->name)  ? $params->name : '';
      $email  = !empty($params->email) ? $params->email : '';
      $status = !empty($params->status) ? $params->status : 1;
      $unixTime = time();
      $token  = encrypt($email . $unixTime);
      $q = "INSERT INTO users (name, email, token, status, uuid)";
      $q .= "VALUES('$name', '$email', '$token', '$status', '$uuid')";
      if(mysqli_query($this->myconn, $q)) {
        $data = array(
            'status' => SUCCESS_OK,
            'token' => $token
        );
      }
      return $data;
    }

    /*
       Function: checkUserinToken
       Check isset token in users table.
    */
    function checkUserinToken($token) {
      $q = "SELECT id FROM users WHERE token='$token' AND status=1 LIMIT 1";
      if(mysqli_query($this->myconn, $q)) {
        $r = mysqli_query($this->myconn, $q) or status500('Error function mysqli_query!');
        if(mysqli_num_rows($r) > 0) {
            return true;
        }
      }
      return false;
    }

    /*
       Function: updateDevice
       Update device_id in users table, It's using for push notification.
    */
    function updateDevice($device_id, $token) {
      $result = array('status' => ERROR_SERVER);
      if($this->checkUserinToken($token)) {
        $q = "UPDATE users SET uuid='{$device_id}' WHERE token='{$token}'";
        if(mysqli_query($this->myconn, $q)) {
            $result = array('status' => SUCCESS_OK);
        }
      }
      return $result;
    }

    /*
       Function: updateDevice
       Update device_id in users table, It's using for push notification.
    */
    function getDeviceUsers() {
      $q = "select uuid FROM users WHERE status=1";
      $r = mysqli_query($this->myconn, $q) or status500('Error function mysqli_query!');
      $data = array();
      if (mysqli_num_rows($r) > 0) {
        while ($row = mysqli_fetch_assoc($r)) {
            $data[] = $row['uuid'];
        }
      }
      return $data;
    }

  function close() {
      mysqli_close($this->myconn);
  }
}

?>
