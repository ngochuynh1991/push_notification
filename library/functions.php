<?php
/**
 * Encryption a string with key
 * @param type $string
 * @param type $key
 * @return type
 */
function encrypt($string, $key = "GhaA5BKeRWXKfH8mbWNmM85F") {
	$s = strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), serialize($string), MCRYPT_MODE_CBC, md5(md5($key)))), '+/=', '-_,');
	return $s;
}

/**
 * Decryption a string with key
 * @param type $string
 * @param type $key
 * @return string
 */
function decrypt($string, $key = "GhaA5BKeRWXKfH8mbWNmM85F") {
	$s = unserialize(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode(strtr($string, '-_,', '+/=')), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
	return $s;
}

/**
 * Return status 500
 * @param type $string
 * @param type $key
 * @return string
 */
function status500($error = '') {
	$result = array('status' => 500);
	if(!empty($error)) {
		$result['messager'] = $error;
	}
	header('Content-type: application/json');
	echo json_encode($result, JSON_UNESCAPED_UNICODE);
	return;
}

/**
 * Return status 400
 * @param type $string
 * @param type $key
 * @return string
 */
function status400($error = '') {
	$result = array('status' => 400);
	if(!empty($error)) {
		$result['messager'] = $error;
	}
	header('Content-type: application/json');
	echo json_encode($result, JSON_UNESCAPED_UNICODE);
	return;
}

/**
 * Return status 200
 * @param type $string
 * @param type $key
 * @return string
 */
function status200($result) {
	header('Content-type: application/json');
	echo json_encode($result, JSON_UNESCAPED_UNICODE); return;
}
