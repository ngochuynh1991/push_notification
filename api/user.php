<?php
require_once ("../library/define.php");
require_once ("../library/db.php");
require_once ("../library/functions.php");
try {
  $db = new DB();
  $method = $_SERVER['REQUEST_METHOD'];
  switch ($method) {
    case 'GET':
      echo 'GET method';
      break;
    case 'PUT':
      echo 'PUT method';
      break;
    case 'POST':
      $body = file_get_contents("php://input");
      $body = str_replace('\"', '"', $body);
      $json_body = json_decode($body);
      if ($json_body !== NULL
        && !empty($json_body->token)
        && !empty($json_body->uuid)
        && !empty($json_body->name)
        && ($json_body->token === KEY_TOKEN)) {
          $uuid = $json_body->uuid;
          if($db->check_uuid($uuid)) {
            $result = $db->getByUid($uuid);
          } else {
            $result = $db->saveUserByParams($json_body);
          }
          status200($result);
      } else {
        status400('The request have problem!');
      }
      break;
  }
} catch (Exception $e) {
    status500($e);
}
?>
