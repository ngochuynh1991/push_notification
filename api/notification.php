<?php
require_once ("../library/define.php");
require_once ("../library/db.php");
require_once ("../library/functions.php");
require_once ("../library/PushNotification.php");
$push = new PushNotification();
$db = new DB();

$method = $_SERVER['REQUEST_METHOD'];
if ($method === 'POST') {
  if (!empty($_POST['token'])
    && !empty($_POST['messager'])
    && $_POST['token'] === KEY_TOKEN) {

    $device_ids = $db->getDeviceUsers();
    if(!empty($device_ids)) {
      $data = $_POST['messager'];
      $message = array('body' => $data);
      $push->push_notification($device_ids, $message);
    } else {
      echo "You do not any device to push!"; return;
    }
  } else {
    echo "Error request"; return;
  }
} else {
  echo "Not isset method POST"; return;
}
?>
