<?php
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n  \"registration_ids\" : [\"fzdkOi-_MR4:APA91bHvBohWqOprrXWGx1ZNzXfOoS2jFZvwo8bC8UoiHHDehgjZlONWbl2LeXKRDA_n2FFR2g8BrGdN96wPePQVGzrSfXDLLBXXR7znXnu6z4Ogi-L0e2I6MKo_enjb7diScCAoxJS8\",\"fzdkOi-_MR4:APA91bHvBohWqOprrXWGx1ZNzXfOoS2jFZvwo8bC8UoiHHDehgjZlONWbl2LeXKRDA_n2FFR2g8BrGdN96wPePQVGzrSfXDLLBXXR7znXnu6z4Ogi-L0e2I6MKo_enjb7diScCAoxJS8\"],\n  \"data\" : {\n  \t\"data\": \"Tuyen lai xe\"\n  }\n}",
  CURLOPT_HTTPHEADER => array(
    "authorization: key=AAAA_32byJA:APA91bFwDdIDC3J0Tom0C9n5FOH2mUmCNm7mOSmj7GjIM9VSMaaVqSZ1Z8WSL999nPv-9oDwqirzq3XNys6uMsfYY_hLNVahOFQmrIPSxLr8ezf3EIutgJwV758WThwybCsD6x4h5fSrnHY3_YWM3eihqA5uILJ3rA",
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
